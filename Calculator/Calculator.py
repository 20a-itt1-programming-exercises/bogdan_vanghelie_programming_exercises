print('Welcome to the Calculator made by Bogdan!')
def calc():
    try:
        firstnumber=input('Enter the first number please: ')
        if firstnumber=='done':
            print('Goodbye')
        else:
            secondnumber=input('Enter the second number please: ')
            if secondnumber=='done':
                print('Goodbye')
            else:
                x=float(firstnumber) #This is the first number
                y=float(secondnumber) #This is the second number
                operator=input('Would like to: 1. add 2. subtract 3. divide or 4. multiply the numbers? ')
                if operator=='done':
                    print('Goodbye')
                else:
                    calculation=int(operator)
                    if calculation==1: #The add function
                        result=x+y
                        print(x,' added with ',y,' = ',result)
                        calc()
                    elif calculation==2: #The subtract function
                        result=x-y
                        print(x,' subtracted from ',y,' = ',result)
                        calc()
                    elif calculation==3: #The divide function
                        if y==0:
                            print('cannot divide by zero, please try again')
                            calc()
                        else:
                            result=x/y
                            print(x,' divided with ',y,' = ',result)
                            calc()
                    elif calculation==4: #The multiply function
                        result=x*y
                        print(x,' multiplied with ',y,' = ',result)
                        calc()
                    else:
                        print('Please choose one of the operations')
                        calc()
    except:
        print('Only numbers and "done" are accepted as input, please try again')
        calc()


calc()