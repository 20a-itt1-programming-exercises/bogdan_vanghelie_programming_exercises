grade=input('Enter score: ')
try:
    fg=float(grade)
    if fg<0.6:
        print('F')
    if fg>=0.6 and fg<=0.7:
        print('D')
    if fg>=0.7 and fg<=0.8:
        print('C')
    if fg>=0.8 and fg<=0.9:
        print('B')
    if fg>=0.9 and fg<=1.0:
        print('A')
    else:
        print('Bad score')
except:
    print('Bad score')
