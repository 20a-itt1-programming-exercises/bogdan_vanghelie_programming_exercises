import re
count = 0
file = input('Enter the file name: ')
hand = open(file)
comb = input('Enter a regular expression: ')

for line in hand:
    line = line.rstrip()
    if re.findall(comb, line):
        count += 1
print(file,'had',str(count),'lines that matched',comb)

