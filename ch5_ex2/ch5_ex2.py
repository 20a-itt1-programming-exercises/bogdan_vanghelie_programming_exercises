maxi=None
mini=None
while True:
    try:
        number=input('Enter a number: ')
        if number=='done':
            print(maxi, mini)
            break
        else:
            number=float(number)
            if maxi is None or number>maxi:
                maxi=number
            if mini is None or number<mini:
                mini=number
    except:
        print('Invalid input')
